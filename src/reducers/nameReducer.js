const initialState = {
  name: 'friend'
};
export default (state = initialState, action) => {
    switch (action.type) {
      case 'CHANGE_NAME':
        return{
          ...state,
          name: action.name
        };
      default:
        return state;
    }
}