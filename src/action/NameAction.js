const setName=(name) =>(dispatch) =>{
  dispatch({
    type:'CHANGE_NAME',
    name:name
  })
};
export default setName