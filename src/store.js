import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import rootReducers from './reducers'


const configStore=(initialStare) =>createStore(
    rootReducers,
    initialStare,
    applyMiddleware(thunk)
);
export default configStore({});