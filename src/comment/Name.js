import React from 'react'
import { bindActionCreators } from "redux";
import {connect} from "react-redux";
import setName from '../action/NameAction'

class Name extends React.Component{
  handleChange(event){
    const name=event.target.value;
    this.props.setName(name);
  }
  render() {
    const {name} = this.props.nameReducer;

    return(
      <div>
        <input type="text" onChange={this.handleChange.bind(this)}/>
        <p>Hello {name}!</p>
      </div>
    )
  }
}

const mapDispatchToProps=(diapatch)=>bindActionCreators({
  setName
},diapatch);

const mapStateToProps=(state) =>({
  nameReducer: state.nameReducer
});

export default connect(mapStateToProps,mapDispatchToProps)(Name);